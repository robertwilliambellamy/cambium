import { config } from './configs/config.js';
import { initial_state } from './configs/store.js';

const MOVEMENT_VECTORS = {
    'N': [ 0,  1],
    'E': [ 1,  0],
    'S': [ 0, -1],
    'W': [-1,  0]
}

// Grid reducer, react to actions and modify state.
export const gridReducer = (state, action) => {

    let new_state = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case 'ON_TICK':
            new_state.tick++;
            return moveRover(new_state);
        case 'START':
            new_state = JSON.parse(JSON.stringify(initial_state));
            new_state.rovers.forEach((m) => {
                validateInstructions(m.movements);
            });
            new_state.ticking = true;
            return new_state;
        case 'PAUSE':
            return new_state;
        case 'RESET':
            new_state = JSON.parse(JSON.stringify(initial_state));
            new_state.ticking = false;
            return new_state;
        default:
            return state;
    }
};

// Determine the new heading of the rover after rotating L/R 90 deg.
const rotateRover = (heading, direction) => {

    let movement_keys = Object.keys(MOVEMENT_VECTORS);
    let heading_index = movement_keys.indexOf(heading);
    heading_index += direction;
    heading_index = (heading_index >= movement_keys.length) ? 0 :
                    (heading_index < 0) ? movement_keys.length - 1 :
                    heading_index;

    return movement_keys[heading_index];
}

// Move the rover along a main compass heading.
const moveRover = (state) => {

    if (state.ticking)  {

        const rover = state.rovers[state.active_rover];
        const move = rover.movements.charAt(state.tick);
        console.log("Move rover", state.active_rover, rover.heading, move);

        // Either rotate L/R or move forward.
        switch(move) {
            case 'L':
                rover.heading = rotateRover(rover.heading, -1);
                break;
            case 'R':
                rover.heading = rotateRover(rover.heading, 1);
                break;
            case 'M':
                const movement = MOVEMENT_VECTORS[rover.heading];
                if (validateMovement(rover.position, movement)) {
                    rover.position[0] += movement[0];
                    rover.position[1] += movement[1];
                }

                break;
            default:
                break;
        }

        // Move to the next rover.
        if (state.tick >= rover.movements.length) {

            state.tick = 0;
            state.active_rover++;
            console.log("Switching to rover", state.active_rover);

            if (state.active_rover >= state.rovers.length) {

                state.ticking = false;
                console.log("End of movements", state);
            }
        }
    }

    return state;
};

// Validate the given movement.
export const validateMovement = (position, movement) => {

    for (const i in position) {

        const np = position[i] + movement[i];
        if (np < 0 || np >= config.grid_size[i])  {
            console.log(config.errors.out_of_bounds);
            return false;
        };
    };

    return true;
}

// Validate instructions.
export const validateInstructions = (instructions) => {

    if (instructions.match('[^LRM]')) {
        throw new Error(config.errors.invalid_instructions);
    }

    return true;
}
