export const config = {
    grid_size: [6, 6],
    tick_rate_ms: 500,
    errors: {
        out_of_bounds: 'Movement out of bounds',
        invalid_instructions: 'Invalid instructions'
    }
};
