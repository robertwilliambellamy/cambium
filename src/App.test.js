import { configure, shallow, mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
configure({ adapter: new Adapter() });

import React from 'react';
import App from './App';

import { validateMovement, validateInstructions } from './GridReducer';
import { config } from './configs/config.js';
import { initial_state } from './configs/store.js';

describe('Test React App', () => {

    it("Renders without crashing", () => {
        shallow(<App />);
    });

    it("Renders all buttons", () => {
         const wrapper = shallow(<App />);
         expect(wrapper.find(".button").length).toEqual(3);
    });

    it("Renders a start button", () => {
         const wrapper = shallow(<App />);
         expect(wrapper.find("#start").length).toEqual(1);
    });

    it("Renders a pause button", () => {
         const wrapper = shallow(<App />);
         expect(wrapper.find("#pause").length).toEqual(1);
    });

    it("Renders a reset button", () => {
         const wrapper = shallow(<App />);
         expect(wrapper.find("#reset").length).toEqual(1);
    });

    it("Renders the correct number of rovers", () => {
         const wrapper = mount(<App />);
         expect(wrapper.find(".rover-container").length).toEqual(initial_state.rovers.length);
    });
});

// Test coordinate validation.
describe('Validate grid coordinates', () => {

    it('Is a valid coordinate ', done => {
        expect(validateMovement([0, 0], [1, 1])).toBe(true);
        done();
    });

    it('Is an invalid coordinate', done => {
        expect(validateMovement(config.grid_size, [1, 1])).toBe(false);
        done();
    });
});

// Test instruction validation.
describe('Validate instructions', () => {

    it('Is a valid instruction set ', done => {
        expect(validateInstructions('LLRMMR')).toBe(true);
        done();
    });

    it('Is an invalid instruction set ', () => {
        expect(() => validateInstructions('AZ')
      ).toThrow(config.errors.invalid_instructions);
    });
});
